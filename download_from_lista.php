#!/usr/bin/php
<?php

include 'src/utils/Utils.php';
include 'src/utils/ShellColors.php';
include 'src/utils/CurlUtils.php';
include 'src/AntenatiCrawler.php';
include 'src/RegistroCrawler.php';


if ($_SERVER['argc'] < 2 || trim($_SERVER['argv'][1]) == '')
{
	echo "Devi specificare un file contenente la lista dei registri da scaricare.\n";
	echo "Per generare il file devi utilizzare l'utility crea_lista_registri.php\n";
	exit(1);
}

$registri = file(trim($_SERVER['argv'][1]), FILE_IGNORE_NEW_LINES);

$numRegistri = count($registri);

echo "Registri trovati: ".$numRegistri."\n";

foreach($registri as $i => $registro)
{
	echo "Processing registro ".ShellColors::getColoredString((string) ($i + 1), 'light_cyan')."/".$numRegistri."...\n";

	/**
	 * se è presente il carattere # ad inizio riga salto il registro corrente
	 * in ogni caso le immagini non verrebbero scaricate se già presenti ma almeno
	 * ri risparmia il crawling (con relativi interval) delle pagine web
	*/
	if (substr($registro, 0, 1) == "#")
	{
		echo "Skipping ".ltrim($registro, '#')."\n";
		continue;
	}
	
	$registroCrawler = new RegistroCrawler($registro);
	$registroCrawler->setDownloadDirectory('downloads/files');
	//$registroCrawler->setDownloadOnlyFirstImage(true);

	$registroCrawler->crawl();

	// segno il registro come completato in modo da poterlo skippare se dovesse rilanciare lo script
	echo shell_exec("/usr/bin/sed -i '' '".($i + 1)."s/^/#/' ".trim($_SERVER['argv'][1]));

	// se esiste il file stop_asap interrompo lo script al termine dell'elaborazione del registro corrente
	if (file_exists("stop_asap"))
	{
		echo "Trovato file stop_asap. Termino l'esecuzione\n";
		break;
	}

	echo "--------------------------------------\n";
}






?>