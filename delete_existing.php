#!/usr/bin/php
<?php

include 'Utils.php';
include 'ShellColors.php';
include 'CurlUtils.php';
include 'AntenatiCrawler.php';
include 'RegistroCrawler.php';


if ($_SERVER['argc'] < 2 || trim($_SERVER['argv'][1]) == '')
{
	echo "Devi specificare un file contenente la lista dei registri da scaricare.\n";
	echo "Per generare il file devi utilizzare l'utility crea_lista_registri.php\n";
	exit(1);
}

$registri = file(trim($_SERVER['argv'][1]), FILE_IGNORE_NEW_LINES);

$numRegistri = count($registri);

$sed = array();

foreach($registri as $i => $registro)
{
	
	$url = "http://www.antenati.san.beniculturali.it/v/";
	$basepath = "/Volumes/ElementsHFS/Antenati/italia/";
	$search = array(
		"stato+civile+italiano",
		"stato+civile+della+restaurazione",
		"stato+civile+napoleonico"
	);
	$replace = array(
		"stato-civile-della-restaurazione-1861-1865",
		"stato-civile-della-restaurazione-1816-1860",
		"stato-civile-napoleonico"
	);

	$registro = strtolower($registro);
	$registro = str_replace($url, $basepath, $registro);
	$registro = str_replace($search, $replace, $registro);
	$registro = str_replace("+", "-", $registro);

	if (file_exists($registro) && is_dir($registro))
		$sed[] = ($i + 1)."d";

}

shell_exec("/usr/bin/sed -i '' '".implode(";", $sed)."' ".trim($_SERVER['argv'][1])." 2>&1");






?>