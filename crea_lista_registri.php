#!/usr/bin/php
<?php

include 'Utils.php';
include 'CurlUtils.php';
include 'AntenatiCrawler.php';
include 'RegistroCrawler.php';
include 'ShellColors.php';

/** 
 * STRUTTURA
 * Archivio+di+Stato+di+Firenze
 *     Stato+civile+napoleonico
 *     Stato+civile+della+restaurazione
 *     Stato+civile+italiano
 *         Brozzi
*/

/** 
 * STRUTTURA
 * Archivio+di+Stato+di+Firenze
 *     Stato+civile+napoleonico
 *         Brozzi
 *     Stato+civile+della+restaurazione
 *         Brozzi
 *         Brozzi+Parrocchia+di+San+Donnino
 *         Brozzi+Parrocchia+di+San+Martino
 *         Brozzi+Parrocchia+di+SantAndrea
 *     Stato+civile+italiano
 *         Brozzi
*/

// SAN DONNINO
/*
$configs = array(
	'Stato+civile+italiano' => array('Brozzi'),

	'Stato+civile+della+restaurazione' => array(
		'Brozzi',
 		'Brozzi+Parrocchia+di+San+Donnino',
 		'Brozzi+Parrocchia+di+San+Martino',
 		'Brozzi+Parrocchia+di+SantAndrea'
	),

	'Stato+civile+napoleonico' => array('Brozzi')
);
*/

// CAMPI BISENZIO
$configs = array(
	'Stato+civile+italiano' => array('Campi'),

	'Stato+civile+della+restaurazione' => array(
		'Campi',
 		'Campi+Bisenzio+Parrocchia+di+San+Lorenzo',
 		'Campi+Bisenzio+Parrocchia+di+San+Martino',
 		'Campi+Bisenzio+Parrocchia+di+Santa+Maria',
 		'Campi+Bisenzio+Parrocchia+di+Santo+Stefano',
 		'Campi+Parrocchia+di+San+Cresci'
	),

	'Stato+civile+napoleonico' => array(
		'Campi',
		'Campi+San+Martino'
	)
);


foreach($configs as $fondo => $arrayComuni)
{
	foreach($arrayComuni as $comune)
	{
		

		$antenatiCrawler = new AntenatiCrawler();

		$antenatiCrawler->setIstituto("Archivio+di+Stato+di+Firenze");
		$antenatiCrawler->setFondo($fondo);
		$antenatiCrawler->setComune($comune);
		$antenatiCrawler->setOutputFile('registri_campi.lst');

		$antenatiCrawler->crawl();
	}
}

/*
// una sola pagina
$urlIndice = "http://www.antenati.san.beniculturali.it/v/Archivio+di+Stato+di+Firenze/Stato+civile+della+restaurazione/Brozzi/Nati+indice/1829/134/";

// 4 pagine
//$urlIndice = "http://www.antenati.san.beniculturali.it/v/Archivio+di+Stato+di+Firenze/Stato+civile+della+restaurazione/Brozzi/Morti/1860/2145/";

$registroCrawler = new RegistroCrawler($urlIndice);
$registroCrawler->setDownloadDirectory(dirname(__FILE__));
$registroCrawler->setDownloadOnlyFirstImage(true);

$registroCrawler->crawl();
*/






?>