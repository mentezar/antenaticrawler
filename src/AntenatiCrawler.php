<?php

class AntenatiCrawler
{

	// se true limita il crawling ad un solo registro
	private $demoMode = false;

	private $crawlInterval = 1; // seconds

	private $host = "http://www.antenati.san.beniculturali.it";

	private $istututo;
	private $fondo;
	private $comune;

	private $outputFile = "";

	private $dom;

	public function setDemoMode($demoMode)
	{
		$this->demoMode = $demoMode;
	}

	public function getDemoMode()
	{
		return $this->demoMode;
	}

	public function setCrawlInterval($interval)
	{
		$this->crawlInterval = $interval;
	}

	public function getCrawlInterval()
	{
		return $this->crawlInterval;
	}

	public function setIstituto($istituto)
	{
		$this->istituto = $istituto;
	}

	public function getIstituto()
	{
		return $this->istututo;
	}

	public function setFondo($fondo)
	{
		$this->fondo = $fondo;
	}

	public function getFondo($fondo)
	{
		return $this->fondo;
	}

	public function setComune($comune)
	{
		$this->comune = $comune;
	}

	public function getComune()
	{
		return $this->comune;
	}

	public function setOutputFile($outputFile)
	{
		$this->outputFile = $outputFile;
	}

	public function getOutputFile()
	{
		return $this->outputFile;
	}

	/**
	 * Metodo principale della classe che effettua il crawling del sito
	 * dell'Archivio di Stato alla ricerca in ordine di:
	 *   1. elenco tipologie registri
	 *   2. elenco anni disponibili per tipologia
	 *   3. registri
	 *
	 * Ogni url (registro) viene salvato in append all'interno del file
	 * specificato nell'attributo outputFile
	 *
	 * @return void
	*/
	public function crawl()
	{

		if (trim($this->outputFile) == '')
		{
			echo "Error: you must specify an output file (eg. registro.lst)\n";
			exit(1);
		}

		echo "Analyzing ...\n";
		echo "Fondo: ".$this->fondo.", Comune: ".$this->comune."\n";

		if (file_exists($this->outputFile))
			unlink($this->outputFile);

		$this->dom = $this->getHtml($this->composeElencoRegistriUrl());

		$registriLinks = array();
		$registriLinks = $this->parseRegistri();

		foreach ($registriLinks as $idxRegistro => $registroLink)
		{
			$anniLinks = array();
			$anniLinks = $this->parseAnniForRegistro($registroLink);
			
			foreach ($anniLinks as $idxAnno => $annoLink)
			{
				$registriForAnnoLinks = array();
				$registriForAnnoLinks = $this->parseRegistriForAnno($annoLink);
				
				if (strpos($registriForAnnoLinks[0], 'jpg') !== false)
				{
					// non è presente il numero interno, siamo già nell'elenco delle immagini
					// casi simili a San Donnino/Sant'Andrea Censimento 1841 (privi del numero interno)
					file_put_contents($this->outputFile, $annoLink."\n", FILE_APPEND);

					if ($idxAnno < (count($anniLinks) - 1))
					{
						//echo "Waiting ".(string) $this->crawlInterval."s before analyzing next registro for anno...\n";
						//echo '.';
						sleep($this->crawlInterval);
					}
				}
				else
				{
					// è presente il numero interno, procedo regolarmente

					foreach ($registriForAnnoLinks as $idxRegistroForAnno => $registroForAnnoLink)
					{
						//echo 'Downloading registro: analizzo registro => '.$registroForAnnoLink."\n";

						// DOWNLOAD DIRETTO
						/*$registroCrawler = new RegistroCrawler($registroForAnnoLink);
						$registroCrawler->setDownloadDirectory('/Volumes/ElementsHFS/Antenati');
						$registroCrawler->setDownloadOnlyFirstImage(true);

						$registroCrawler->crawl();
						*/

						file_put_contents($this->outputFile, $registroForAnnoLink."\n", FILE_APPEND);

						if ($idxRegistroForAnno < (count($registriForAnnoLinks) - 1))
						{
							//echo "Waiting ".(string) $this->crawlInterval."s before analyzing next registro for anno...\n";
							//echo '.';
							sleep($this->crawlInterval);
						}
					}
				}

				if ($idxAnno < (count($anniLinks) - 1))
				{
					//echo "Waiting ".(string) $this->crawlInterval."s before analyzing next anno...\n";
					//echo '.';
					sleep($this->crawlInterval);
				}

				if ($this->demoMode)
				{
					echo "demoMode == true. Interrompo!\n";
					break(2);
				}

			}

			if ($idxRegistro < (count($registriLinks) - 1))
			{
				//echo "Waiting ".(string) $this->crawlInterval."s before analyzing next registro...\n";
				//echo '.';
				sleep($this->crawlInterval);
			}

		}

	}

	/**
	 * Download della pagina specificata in $url, salvataggio del codice html
	 * e ritorno del relativo oggetto DOM
	 *
	 * @param String $url Url della pagina da scaricare
	 * @return DOMDocument
	*/
	private function getHtml($url)
	{
		$htmlIndice = "";

		$htmlIndice = CurlUtils::getPage($url, true);

		$dom = new DOMDocument;
		$dom->loadHTML($htmlIndice);

		return $dom;

	}

	/**
	 * Compone la stringa relativa all'url contenente l'elenco dei registri
	 *
	 * @return String
	*/
	private function composeElencoRegistriUrl()
	{
		return $this->host.'/v/'.$this->istituto.'/'.$this->fondo.'/'.$this->comune.'/';
	}

	/**
	 * Parsing della pagina dei registri alla ricerca di link delle varie
	 * tipologie di registri
	 *
	 * @return Array of Strings
	*/
	private function parseRegistri()
	{
		$links = array();

		$xpath = new DOMXPath($this->dom);
		$nodes = $xpath->query('//p[@class="giTitle"]/a/@href');
		foreach($nodes as $href) {
		    $links[] = $this->host.$href->nodeValue;                       // echo current attribute value
		    //$href->nodeValue = 'new value';              // set new attribute value
		    //$href->parentNode->removeAttribute('href');  // remove attribute
		}

		return $links;

	}

	/**
	 * Parsing della pagina della singola tipologia di registro alla ricerca di link
	 * dei vari anni
	 *
	 * @return Array of Strings
	*/
	private function parseAnniForRegistro($registroLink)
	{

		$dom = $this->getHtml($registroLink);

		$links = array();

		$xpath = new DOMXPath($dom);
		$nodes = $xpath->query('//p[@class="giTitle"]/a/@href');
		foreach($nodes as $href) {
		    $links[] = $this->host.$href->nodeValue;                       // echo current attribute value
		    //$href->nodeValue = 'new value';              // set new attribute value
		    //$href->parentNode->removeAttribute('href');  // remove attribute
		}

		return $links;

	}

	/**
	 *
	 * @return Array of Strings
	*/
	private function parseRegistriForAnno($annoLink)
	{

		$dom = $this->getHtml($annoLink);

		$links = array();

		$xpath = new DOMXPath($dom);
		$nodes = $xpath->query('//p[@class="giTitle"]/a/@href');
		foreach($nodes as $href) {
		    $links[] = $this->host.$href->nodeValue;                       // echo current attribute value
		    //$href->nodeValue = 'new value';              // set new attribute value
		    //$href->parentNode->removeAttribute('href');  // remove attribute
		}

		return $links;

	}



}


?>