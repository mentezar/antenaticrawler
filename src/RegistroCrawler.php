<?php


class RegistroCrawler
{

	private $crawlInterval = 1; // seconds
	private $downloadInterval = 1; // seconds
	
	private $downloadDirectory = null;
	private $downloadDirectoryChmod;
	private $fullLocalPath = "";

	private $url = "";
	private $host = "";
	private $dom;

	private $metadata = array();
	
	private $pages;
	private $linksInIndexPages = array();
	private $highResolutionImagesUrls = array();

	// debug purpose only
	private $downloadOnlyFirstImage = false;


	public function __construct($url)
	{
		// we add the trailing slash if missing in url
		if (substr($url, -1) != '/')
			$url = $url.'/';

		$this->url = $url;

		$this->host = parse_url($url, PHP_URL_SCHEME).'://'.parse_url($url, PHP_URL_HOST);

	}

	private function checkConfiguration()
	{
		if (is_null($this->downloadDirectory))
		{
			echo "Error: you must set the downlaod directory. Use the method setDownloadDirectory.\n";
			exit(1);
		}

		if (!file_exists($this->downloadDirectory) || !is_dir($this->downloadDirectory))
		{
			echo "Error: download directory specified must exists.\n";
			exit(1);
		}
		
	}

	public function crawl()
	{
		$this->checkConfiguration();

		echo "Analyzing url...\n";
		$this->dom = $this->getHtml($this->url);
		
		echo "Getting metadata...\n";
		$this->parseMetadata();
		$this->pages = $this->parseNumberOfPages();

		$this->composeFullLocalPath();
		
		echo "Pages found: ".$this->pages."\n";
		echo "Starting crawler...\n";
		echo "\tNazione: ".ShellColors::getColoredString($this->metadata['nazione'], 'yellow')."\n";
		echo "\tIstituto: ".ShellColors::getColoredString($this->metadata['istituto'], 'yellow')."\n";
		echo "\tFondo: ".ShellColors::getColoredString($this->metadata['fondo'], 'yellow')."\n";
		echo "\tComune: ".ShellColors::getColoredString($this->metadata['comune'], 'yellow')."\n";
		echo "\tRegistro: ".ShellColors::getColoredString($this->metadata['registro'], 'yellow')."\n";
		echo "\tAnno: ".ShellColors::getColoredString($this->metadata['anno'], 'yellow')."\n";
		
		if (isset($this->metadata['numeroInterno']))
			echo "\tNumero interno: ".ShellColors::getColoredString($this->metadata['numeroInterno'], 'yellow')."\n";
		else
			echo "\tNumero interno: ".ShellColors::getColoredString("-", 'yellow')."\n";
		
		for($p = 1; $p <= $this->pages; $p++)
		{
			echo "Analyzing and parsing index page ".$p."/".$this->pages."...\n";
			
			
			if ($p > 1)
				$this->dom = $this->getHtml($this->url.'?g2_page='.(string) $p);

			$this->linksInIndexPages = array_merge($this->linksInIndexPages, $this->parseLinksInIndexPage());

			if ($p > 1)
			{
				//echo "Waiting ".(string) $this->crawlInterval."s before analyzing the next page...\n";
				//echo '.';
				sleep($this->crawlInterval);
			}

		}

		$numImages = count($this->linksInIndexPages);
		echo "Images found: ".(string) $numImages."\n";
 
		$skipDownloadInterval = false;

		foreach ($this->linksInIndexPages as $i => $imageUrl)
		{
			$skipDownloadInterval = false;

			$this->dom = $this->getHtml($imageUrl);
			
			$this->highResolutionImagesUrls[$i] = $this->parseImageUrl();
			
			$chmod = substr(sprintf('%o', fileperms($this->downloadDirectory)), -4);

			if (!file_exists($this->fullLocalPath))
			{
				if (!mkdir($this->fullLocalPath, octdec($chmod), true))
				{
					echo "No permissions to create directory in ".$this->downloadDirectory."\n";
					exit(1);
				}
			}

			$destinationFilenameWithPath = $this->fullLocalPath.'/'.($i + 1).'.jpg';

			if (file_exists($destinationFilenameWithPath))
			{
				$skipDownloadInterval = true;
				echo "File $destinationFilenameWithPath already exists, skipping...\n";
			}
			else 
			{
				echo "Downloading image ".(string) ($i + 1)."/". (string) $numImages."\n";
				CurlUtils::downloadFile($this->highResolutionImagesUrls[$i], $destinationFilenameWithPath);
			}
			
			if ($this->downloadOnlyFirstImage === true && $i == 0)
			{
				break;
			}

			//echo "Waiting ".(string) $this->downloadInterval."s before downloading next high resolution image...\n";
			//echo '.';
			if (!$skipDownloadInterval)
				sleep($this->downloadInterval);
		}

	}

	private function composeFullLocalPath()
	{
		$fullLocalPath = $this->downloadDirectory.'/';

		$slugifiedMetadata = array();

		foreach($this->metadata as $metadata)
		{
			$slugifiedMetadata[] = Utils::slugify($metadata, false);
		}
		
		$fullLocalPath .= implode('/', $slugifiedMetadata);

		$this->fullLocalPath = $fullLocalPath;
	}

	/**
	 *
	 * @return DOMDocument
	*/
	private function getHtml($url)
	{
		$htmlIndice = CurlUtils::getPage($url, true);

		$dom = new DOMDocument;
		$dom->loadHTML($htmlIndice);

		return $dom;

	}

	private function parseMetadata()
	{
		$dati = array();

		$xpath = new DOMXPath($this->dom);
		$nodes = $xpath->query('//div[@class="gbBreadCrumb"]/div/a');
		foreach($nodes as $node) {
		    $dati[] = $node->nodeValue;                       // echo current attribute value
		    //$href->nodeValue = 'new value';              // set new attribute value
		    //$href->parentNode->removeAttribute('href');  // remove attribute
		}

		$nodes = $xpath->query('//span[@class="BreadCrumb-13"]');
		
		foreach($nodes as $node) {
			$dati[] = $node->nodeValue;
		}

		$this->metadata['nazione'] = trim($dati[0]);
		$this->metadata['istituto'] = trim($dati[1]);
		$this->metadata['fondo'] = trim($dati[2]);
		$this->metadata['comune'] = trim($dati[3]);
		$this->metadata['registro'] = trim($dati[4]);
		
		if (isset($dati[5]))
		{
			$this->metadata['anno'] = trim($dati[5]);
			$this->metadata['numeroInterno'] = trim($dati[6]);
		}
		else
		{
			$nodes = $xpath->query('//span[@class="BreadCrumb-11"]');
			foreach($nodes as $node) {
			    $dati[] = $node->nodeValue;
			}
			$this->metadata['anno'] = trim($dati[5]);
		}
		
	}

	private function parseNumberOfPages()
	{
		$pages = 1;
		$xpath = new DOMXPath($this->dom);
		$nodes = $xpath->query('//div[@class="next-and-last no-previous"]/a[@class="last"]/@href');

		if ($nodes->length > 0)
		{
			foreach($nodes as $href) {

			    $href = $href->nodeValue;                       // echo current attribute value
			    //$href->nodeValue = 'new value';              // set new attribute value
			    //$href->parentNode->removeAttribute('href');  // remove attribute
			}

			$matches = null;
			$returnValue = preg_match('/g2_page=([0-9]+)/', $href, $matches);
			if ($returnValue)
				$pages = $matches[1];
		}

		return $pages;

	}

	private function parseLinksInIndexPage()
	{
		$links = array();

		$xpath = new DOMXPath($this->dom);
		$nodes = $xpath->query('//p[@class="giTitle"]/a/@href');
		foreach($nodes as $href) {
		    $links[] = $this->host.$href->nodeValue;                       // echo current attribute value
		    //$href->nodeValue = 'new value';              // set new attribute value
		    //$href->parentNode->removeAttribute('href');  // remove attribute
		}

		return $links;

	}

	private function parseImageUrl()
	{
		$url = "";

		$xpath = new DOMXPath($this->dom);
		$nodes = $xpath->query('//div[@id="gsImageView"]/a/@href');
		foreach($nodes as $href) {
			//$matches = null;
			//$returnValue = preg_match('/g2_itemId=([0-9]+)/', $href->nodeValue, $matches);
			//if ($returnValue)
			//	$url = $matches[1];
			//else
			//	$url = "";
			$url = $href->nodeValue;
		}

		return $url;
	}

	private function downloadHighResolutionImage($url)
	{
		//$finalDirectory = $this->downloadDirectory.'/'.Utils::slugify(implode('/', $this->metadata));
		//CurlUtils::downloadFile($url, $this->downloadDirectory);
	}


	public function getMetadata()
	{
		return $this->metadata;
	}

	public function getNumberOfPages()
	{
		return $this->pages;
	}

	public function getLinksInIndexPages()
	{
		return $this->linksInIndexPages;
	}

	public function setCrawlInterval($interval)
	{
		$this->crawlInterval = $interval;
	}

	public function getCrawlInterval()
	{
		return $this->crawlInterval;
	}

	public function setDownloadDirectory($directory)
	{
		$this->downloadDirectory = rtrim($directory,"/");;
	}

	public function getDownloadDirectory()
	{
		return $this->downloadDirectory;
	}

	public function setDownloadOnlyFirstImage($downloadOnlyFirstImage)
	{
		$this->downloadOnlyFirstImage = $downloadOnlyFirstImage;
	}

	public function getDownloadOnlyFirstImage()
	{
		return $this->downloadOnlyFirstImage;
	}


}


?>
