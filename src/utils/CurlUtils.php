<?php

class CurlUtils
{
	public static function getPage($url, $retryIfEmpty = false, $secondsBeforRetry = 1)
	{
		$output = "";

		// messo while perché alle volte la get restituisce pagina vuota per colpa del server
		while (empty($output))
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0');
			//curl_setopt($ch, CURLOPT_USERPWD, 'superuser:superuser');
			curl_setopt($ch, CURLOPT_TIMEOUT, 5);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$output = curl_exec($ch);
			curl_close($ch);

			if ($retryIfEmpty && empty($output))
			{
				echo ShellColors::getColoredString("Restituita pagina vuota. Retry:", 'red')." $url\n";
				sleep($secondsBeforRetry);
			}
		}

		return $output;

	}

	public static function downloadFile($url, $destination)
	{
		set_time_limit(0);
		$fp = fopen($destination, 'w+');//This is the file where we save the information
		$ch = curl_init(str_replace(" ", "%20", $url));//Here is the file we are downloading, replace spaces with %20
		
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0');

		$responseHeaders = array();

		curl_setopt($ch, CURLOPT_HEADERFUNCTION, function ($ch, $header) use (&$responseHeaders) {
			
			$sourceUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
	        $responseHeaders[] = $header;
	        
	        return strlen($header);
		});

		curl_setopt($ch, CURLOPT_TIMEOUT, 50);
		curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_exec($ch); // get curl response
		curl_close($ch);
		fclose($fp);

		$attachInfo = array();

		foreach($responseHeaders as $header)
		{
			$header = trim($header);

			//echo "HEADER => ".$header."\n";

			// Content-Disposition: inline; filename="005176460_00100.jpg"
			/*
			if (stripos($header, 'filename') !== FALSE)
			{
				$matches = null;
				if (preg_match('/filename="(.*)"/', $header, $matches))
					$filename = $matches[1];
			}

			// Content-Type: image/jpeg
			if (stripos($header, 'Content-Type') !== FALSE)
			{
				$matches = null;
				if (preg_match('/Content-Type: (.*)/', $header, $matches))
					$contentType = $matches[1];
			}
			*/
			if (preg_match("/Content-Length: (\d*)/i", $header, $matches)) {
				$attachInfo['size'] = $matches[1];
			} elseif (preg_match('/Content-Disposition:.*?filename="(.*)"/i', $header, $matches)) {
				$attachInfo['filename'] = $matches[1];
			} elseif (preg_match("/Content-Type: ([^; ]*)/i", $header, $matches)) {
				$attachInfo['type'] = $matches[1];
			}

		}

		/* TODO valutare se usare il seguente naming schema per il file
		filename: progressivo immagine (come è attualmente)
		estensione: rename aggiungendo l'estensione tramit Utils::getExtensionFromContentType($attachInfo['type']);
		*/

		/*
		print_r($attachInfo);
		Array
		(
		    [filename] => 005176460_00100.jpg
		    [type] => image/jpeg
		    [size] => 1072986
		)
		*/
	}

	/* NOT USED
	public static function readHeader($ch, $header)
	{
        global $responseHeaders;
        $url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        $responseHeaders[$url][] = $header;
        print_r($responseHeaders);
    	return strlen($header);
	}
	*/
}

?>