#!/usr/bin/php
<?php

include 'src/utils/Utils.php';
include 'src/utils/CurlUtils.php';
include 'src/AntenatiCrawler.php';
include 'src/RegistroCrawler.php';
include 'src/utils/ShellColors.php';

/** 
 * STRUTTURA
 * Archivio+di+Stato+di+Firenze
 *     Stato+civile+napoleonico
 *     Stato+civile+della+restaurazione
 *     Stato+civile+italiano
 *         Brozzi
*/

/** 
 * STRUTTURA
 * Archivio+di+Stato+di+Firenze
 *     Stato+civile+napoleonico
 *         Brozzi
 *     Stato+civile+della+restaurazione
 *         Brozzi
 *         Brozzi+Parrocchia+di+San+Donnino
 *         Brozzi+Parrocchia+di+San+Martino
 *         Brozzi+Parrocchia+di+SantAndrea
 *     Stato+civile+italiano
 *         Brozzi
*/

// SAN DONNINO

$configs = array(
	'Stato+civile+italiano' => array('Brozzi'),

	'Stato+civile+della+restaurazione' => array(
		'Brozzi',
 		'Brozzi+Parrocchia+di+San+Donnino',
 		'Brozzi+Parrocchia+di+San+Martino',
 		'Brozzi+Parrocchia+di+SantAndrea'
	),

	'Stato+civile+napoleonico' => array('Brozzi')
);





foreach($configs as $fondo => $arrayComuni)
{
	foreach($arrayComuni as $comune)
	{
		

		$antenatiCrawler = new AntenatiCrawler();

		$antenatiCrawler->setDemoMode(true);
		$antenatiCrawler->setIstituto("Archivio+di+Stato+di+Firenze");
		$antenatiCrawler->setFondo($fondo);
		$antenatiCrawler->setComune($comune);
		$antenatiCrawler->setOutputFile('downloads/registri/registri_san_donnino.lst');

		$antenatiCrawler->crawl();

		if ($antenatiCrawler->getDemoMode())
		{
			echo "Interrompo per flag demoMode!\n";
			break(2);
		}
	}
}

/*
// una sola pagina
$urlIndice = "http://www.antenati.san.beniculturali.it/v/Archivio+di+Stato+di+Firenze/Stato+civile+della+restaurazione/Brozzi/Nati+indice/1829/134/";

// 4 pagine
//$urlIndice = "http://www.antenati.san.beniculturali.it/v/Archivio+di+Stato+di+Firenze/Stato+civile+della+restaurazione/Brozzi/Morti/1860/2145/";

$registroCrawler = new RegistroCrawler($urlIndice);
$registroCrawler->setDownloadDirectory(dirname(__FILE__));
$registroCrawler->setDownloadOnlyFirstImage(true);

$registroCrawler->crawl();
*/






?>