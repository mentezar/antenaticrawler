<?php

class AntenatiCrawlerTest extends PHPUnit_Framework_TestCase
{
	private $antenatiCrawler;
	private $testUrl = "http://www.antenati.san.beniculturali.it/v/Archivio+di+Stato+di+Firenze/Stato+civile+italiano/Brozzi/";

	public function setUp()
	{
		$this->antenatiCrawler = new AntenatiCrawler();
		$this->antenatiCrawler->setIstituto("Archivio+di+Stato+di+Firenze");
		$this->antenatiCrawler->setFondo("Stato+civile+italiano");
		$this->antenatiCrawler->setComune("Brozzi");

		
	}

	/**
	 * @test
	*/
	public function composeElencoRegistriUrlTest()
	{
		
		$method = $this->getPrivateMethod("AntenatiCrawler", "composeElencoRegistriUrl");
		$result = $method->invoke($this->antenatiCrawler); 

		$this->assertEquals($this->testUrl, $result);
	}

	/**
	 * @test
	*/
	public function getHtmlTest()
	{

		$method = $this->getPrivateMethod("AntenatiCrawler", "getHtml");
		$result = $method->invokeArgs($this->antenatiCrawler, array($this->testUrl));
		$this->setPrivateProperty($this->antenatiCrawler, "dom", $result);
		
		$this->assertInstanceOf("DOMDocument", $result);

		$classname = 'messages error';
		$xpath = new DOMXPath($result);
		$results = $xpath->query("//*[@class='" . $classname . "']");

		$this->assertEquals(0, $results->length);
		
	}

	/**
	 * @test
	*/
	public function parseRegistriTest()
	{
		$method = $this->getPrivateMethod("AntenatiCrawler", "getHtml");
		$result = $method->invokeArgs($this->antenatiCrawler, array($this->testUrl));
		$this->setPrivateProperty($this->antenatiCrawler, "dom", $result);

		$method = $this->getPrivateMethod("AntenatiCrawler", "getHtml");
		$result = $method->invokeArgs($this->antenatiCrawler, array($this->testUrl));

		$method = $this->getPrivateMethod("AntenatiCrawler", "parseRegistri");
		$registriLinks = $method->invoke($this->antenatiCrawler);
		
		$this->assertGreaterThan(0, count($registriLinks));
	}

	/**
	 * @test
	*/
	public function parseAnniForRegistroTest()
	{
		$method = $this->getPrivateMethod("AntenatiCrawler", "getHtml");
		$result = $method->invokeArgs($this->antenatiCrawler, array($this->testUrl));
		$this->setPrivateProperty($this->antenatiCrawler, "dom", $result);

		$method = $this->getPrivateMethod("AntenatiCrawler", "getHtml");
		$result = $method->invokeArgs($this->antenatiCrawler, array($this->testUrl));

		$method = $this->getPrivateMethod("AntenatiCrawler", "parseRegistri");
		$registriLinks = $method->invoke($this->antenatiCrawler);
		
		$method = $this->getPrivateMethod("AntenatiCrawler", "parseAnniForRegistro");
		$anniLinks = $method->invokeArgs($this->antenatiCrawler, array($registriLinks[0]));

		$this->assertGreaterThan(0, count($anniLinks));
	}

	/**
	 * @test
	*/
	public function parseRegistriForAnnoTest()
	{
		$method = $this->getPrivateMethod("AntenatiCrawler", "getHtml");
		$result = $method->invokeArgs($this->antenatiCrawler, array($this->testUrl));
		$this->setPrivateProperty($this->antenatiCrawler, "dom", $result);

		$method = $this->getPrivateMethod("AntenatiCrawler", "getHtml");
		$result = $method->invokeArgs($this->antenatiCrawler, array($this->testUrl));

		$method = $this->getPrivateMethod("AntenatiCrawler", "parseRegistri");
		$registriLinks = $method->invoke($this->antenatiCrawler);
		
		$method = $this->getPrivateMethod("AntenatiCrawler", "parseAnniForRegistro");
		$anniLinks = $method->invokeArgs($this->antenatiCrawler, array($registriLinks[0]));

		$method = $this->getPrivateMethod("AntenatiCrawler", "parseRegistriForAnno");
		$registriForAnno = $method->invokeArgs($this->antenatiCrawler, array($anniLinks[0]));

		$this->assertGreaterThan(0, count($registriForAnno));
	}

	/**
	 * @test
	*/
	public function crawlTest()
	{
		$outputFile = "tests/registri_san_donnino.lst";
		$this->antenatiCrawler->setOutputFile($outputFile);
		$this->antenatiCrawler->crawl();
		$this->assertTrue(file_exists($outputFile));
		$this->assertGreaterThan(0, filesize($outputFile));

		if (file_exists($outputFile))
			unlink($outputFile);
	}

	public function getPrivateMethod($className, $methodName)
	{
		$reflector = new ReflectionClass($className);
		$method = $reflector->getMethod($methodName);
		$method->setAccessible(true);
 
		return $method;
	}

	public function getPrivateProperty($className, $propertyName)
	{
		$reflector = new ReflectionClass($className);
		$property = $reflector->getProperty($propertyName);
		$property->setAccessible(true);
 
		return $property;
	}

	public function setPrivateProperty(&$classInstance, $propertyName, $propertyValue)
	{
		$reflectorProperty = new ReflectionProperty(get_class($classInstance), $propertyName);
		$reflectorProperty->setAccessible(true);
		$reflectorProperty->setValue($classInstance, $propertyValue);
		
		// other method
		//$reflection = new \ReflectionClass($classInstance);
		//$property = $reflection->getProperty($propertyName);
		//$property->setAccessible(true);
		//$property->setValue($classInstance, $propertyValue);
	}
}

?>