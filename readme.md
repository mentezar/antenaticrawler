# AntenatiCrawler
Dovendo fare una ricerca storica personale (ricostruzione albero genealogico) sul portale "Antenati" dell'Archivio di stato di Firenze mi sono accorto che il sito, oltre ad essere lento, risultava poco fruibile ai fini di una ricerca efficace.
Perciò ho deciso di creare un crawler che, a partire da alcuni parametri di input (fondo e comune), indicizzasse tutti i registri presenti e li scaricasse integralmente su filesystem ricreando la stessa struttura presente sul portale.

Il crawler è stato realizzato senza troppe pretese a partire da un'esigenza. Man mano si è evoluto e migliorato ma richiederebbe un refactor per renderlo più efficiente e facilmente testabile.

Il crawler è composto da due componenti principali:
 1. componente per la creazione di un file contenente l'elenco dei registri da scaricare.
 2. componente che leggendo il file dei registri effettua il vero e proprio download della immagini
 
La componente 1 viene invocata tramite l'esecuzione dello script crea_lista_registri_SAN_DONNINO.php (crawling limitato ad un registro tramite demoMode attivato)

La componente 2 viene invocata tramite l'esecuzione dello script download_from_lista.php. Analizza il file generato dalla componente 1 ed effettua il download di tutti i registri ivi contenuti ricreando fedelmente la struttura su filesystem.
Dato il numero elefato di files, il download richiede molto tempo (sono implementati anche degli sleep). Pertanto, qualora si dovesse interrompere l'esecuzione dello script per proseguirla in un secondo momento senza un'interruzione CTRL+C è sufficiente creare un file nella root del progetto chiamato stop_asap
```sh
$ touch stop_asap
```
Il download terminerà dopo aver scaricato l'ultimo file del registro corrente. Al successivo riavvio dello script dovrà essere cancellato preventivamente il file stop_asap.
Man mano che viene scaricato completamente un registro, la relativa riga del file di origine (contenente i link) viene commentata tramite il carattere cancelletto in modo da saltarla in caso di interruzione e riavvio dello script.

### demoMode
Utilizzando lo script crea_lista_registri_SAN_DONNINO.php è attivo il demoMode che limita il crawling ad un unico registro.

### Testing
Attualmente è presente una classe di test per la classe AntenatiCrawler che include un misto di unit e functional test. Per un migliore e più agevolte testing andrebbe fatto un refactor del codice.
```sh
$ phpunit -c phpunit.xml
```

### Todos
 - spostare tutto ciò che è configurazione all'interno di un apposito file di configurazione, probabilmente yaml
 - scrivere classe di test per la classe RegistroCrawler
 - refactor del codice per rendere più agevole il testing

### Attività successiva
Dato che il numero di immagini scaricate dal crawler è elevato, risulta oneroso in termini di tempo visionare ogni singolo file. Una possibile soluzione è quella di implementare algoritmi di Handwriting Recognition tramite Python e OpenCV. L'Handwriting Recognition non è di per sè difficile se non fosse che:
 - alcuni documenti sono poco leggibili
 - si tratta di riconoscere scritte non in stampatelli bensì in corsivo con l'ulteriore difficoltà che è un corsivo con stile di scrittura "del passato"